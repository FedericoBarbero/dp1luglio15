<?php
include 'functions.php';
if (! isset ( $_COOKIE ['testCookie'] )) {
	setcookie ( 'testCookie', 'enabled' );
	$dest = buildNewDestUrlSource ( $_SERVER, "check.php" );
	header ( "Location: " . $dest );
}
?>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="it">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<title>Hall Reservation</title>
<script type="text/javascript" src="MyScripts.js"></script>
<link href="MyStyle.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="navbar">
		<ul class="navbar">
			<li><a id="navlink" href="index.php">Home</a></li>
			<li><a id="navlink" href="signup.php">Sign Up</a></li>
			<li><a id="navlink" href="signin.php">Sign In</a></li>
			<li><a id="navlink" href="logout.php">Log Out</a></li>
			<li><a id="navlink" href="personalreservation.php">Personal Page</a></li>
		</ul>
	</div>
	<div class="header">
		<div id="title">
			<h1>Hotel Conference Hall Booking Site</h1>
		</div>
		<div id="page">
			<h2>Home</h2>
		</div>
	</div>
	<noscript id="alert">Sorry, your browser does not support or has disabled
		Javascript! Please consider changing browser or turning it back on.</noscript>
	<div class="content">
	<table class="table">
		<tr id="header">
			<th>#</th>
			<th>Number Of Participants</th>
			<th>Start Time</th>
			<th>End Time</th>
		</tr>		
				<?php
					$query = "SELECT NOfParticipants, StartTime, EndTime FROM bookings ORDER BY NOfParticipants DESC";
					$res = getQuery ($db, $query);
					
					$row = mysqli_fetch_array ( $res );
					$i=0;
					while ( $row != NULL ) {
				?>    
	    <tr id="data">
				<td><?php 
					echo ($i);
					$i++;
					?></td>
				<td><?php
					echo ($row ["NOfParticipants"]);
					?></td>
				<td><?php
					$st = formatTimeHhMm($row ["StartTime"]);
					echo ($st);
					?></td>
				<td><?php
					$et = formatTimeHhMm($row ["EndTime"]);
					echo ($et);
					?></td>
		</tr>

    <?php
				$row = mysqli_fetch_array ( $res );
			}
			
			mysqli_free_result ( $res );
			?>
    </table>

	</div>
</body>
</html>