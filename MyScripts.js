/***********************************************************
 * JS file created by Federico Barbero - Matricola 220352  *
 ***********************************************************/
function deleteSelected(formname, checktoggle)
{
  var checkboxes = new Array(); 
  checkboxes = document[formname].getElementsByTagName('input');
 
  for (var i=0; i<checkboxes.length; i++)  {
    if (checkboxes[i].type == 'checkbox')   {
      checkboxes[i].checked = checktoggle;
    }
  }
}

function checkNewBooking(){
	var part = document.forms["new_booking"]["#OfParticipants"].value;
	var startH = parseInt(document.forms["new_booking"]["startHour"].value);
	var startM = parseInt(document.forms["new_booking"]["startMinutes"].value);
	var endH = parseInt(document.forms["new_booking"]["endHour"].value);
	var endM = parseInt(document.forms["new_booking"]["endMinutes"].value);
	var start = startH*60+startM;
	var end = endH*60+endM;
	if(part == null || part=="" || isNaN(part) || part < 0 || part >100 || end <= start){
		alert("Incorrect input data!");
		return false;
	}
	
}

function checkSignIn(myForm){
	var x = document.forms[myForm]["username"].value;
    if (x == null || x == "") {
        alert("Username must be filled out");
        return false;
    }
    var x = document.forms[myForm]["password"].value;
    if (x == null || x == "") {
        alert("Password must be filled out");
        return false;
    }
}
