<?php 
	include 'functions.php';
	if(!isset($_COOKIE['testCookie']))
	{
		setcookie('testCookie','enabled'); 
		$dest = buildNewDestUrlSource($_SERVER, "check.php");
		header("Location: " . $dest);
	}
	session_start();
	if(isset($_SESSION["S220352user"]))
	{
		$dest = buildNewDestUrl($_SERVER, "personalreservation.php");
		header("Location: " . $dest);
		exit();
	}
	else
	{
		session_unset();
		session_destroy();
	}
?>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="it">
	<head>
		<meta content="text/html; charset=utf-8" http-equiv="content-type">
		<title>Hall Reservation</title>
		<script type="text/javascript" src="MyScripts.js"></script>
		<link href="MyStyle.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="navbar">
			<ul class="navbar">
				<li><a id="navlink" href="index.php">Home</a></li>
				<li><a id="navlink" href="signup.php">Sign Up</a></li>
				<li><a id="navlink" href="signin.php">Sign In</a></li>
				<li><a id="navlink" href="logout.php">Log Out</a></li>
				<li><a id="navlink" href="personalreservation.php">Personal Page</a></li>
			</ul>
		</div>
		<div class="header">
			<div id="title">
				<h1>Hotel Conference Hall Booking Site</h1>
			</div>
			<div id="page">
				<h2>Sign Up</h2>
			</div>		
		</div>
		<noscript id="alert">Sorry, your browser does not support or has disabled Javascript! Please consider changing browser or turning it back on.</noscript>
		<div class="content">
			<form action="validate.php" method="get" class="login" onsubmit="return checkSignIn('signup')" name="signup">
				<h3>Register Now</h3>
				<label>
					<span>Username: </span>
					<input type="text" id="user" name="username" title="Insert your username" placeholder="Insert your username">
				</label>				
				<label>
					<span>Password: </span>
					<input type="password" id="pass" name="password" placeholder="Insert your password" title="Insert your password">
				</label>
				<input type="hidden" name="source" value="signup.php">
				<input type="submit" id="confirm" value="Confirm">
			</form>
		</div>
	</body>
</html>