<?php
	include 'functions.php'; 
	
	if(isset($_GET['source']))
		$source = $_GET['source'];
	else 
	{
		$err = buildNewDestUrl($_SERVER, "error.php");
		header("Location: " . $err);
	}
	
	if($source == "signup.php")
	{
	
		if(isset($_GET["username"]) && isset($_GET["password"]))
		{
			$username = $_GET["username"];
			$password = $_GET["password"];
			
			if(usernameValid($db, $username))
			{
				if(insertUser($db, $username, $password))
				{
					$succ = buildNewDestUrl($_SERVER, "signin.php");
					header("Location: " . $succ);
				}
				else
				{
					$err = buildNewDestUrl($_SERVER, 'error_invalid.php');
					header("Location: " . $err);
				}
			}
			else
			{
				$err = buildNewDestUrl($_SERVER, "error_invalid.php");
				header("Location: " . $err);
			}
		}
		else 
		{		
			$dest = buildNewDestUrl($_SERVER, $source);
			header("Location: " . $dest);
		}
	}
	elseif ($source == "signin.php")
	{
		if(isset($_GET["username"]) && isset($_GET["password"]))
		{
			$username = $_GET["username"];
			$password = $_GET["password"];

			if(!strcmp($username,"") || !strcmp($password,""))
			{
				$dest = buildNewDestUrl($_SERVER, "error_invalid.php");
				header("Location: " . $dest);
			}
			if(utenteValido($db, $username, $password))
			{
				if(checkSession())
				{
					$dest = buildNewDestUrl($_SERVER, "personalreservation.php");
					header("Location: " . $dest);
				}
				else 
				{
					$dest = buildNewDestUrl($_SERVER, "error_invalid.php");
					header("Location: " . $dest);
				}
			}
			else
				{
					$dest = buildNewDestUrl($_SERVER, "error_invalid.php");
					header("Location: " . $dest);
				}
		}
		else
		{
			$dest = buildNewDestUrl($_SERVER, $source);
			header("Location: " . $dest);
		}
	}
?>