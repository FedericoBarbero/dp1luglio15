<?php
include 'functions.php';
if (! isset ( $_COOKIE ['testCookie'] )) {
	setcookie ( 'testCookie', 'enabled' );
	$dest = buildNewDestUrlSource ( $_SERVER, "check.php" );
	header ( "Location: " . $dest );
}
if (! checkSession ()) {
	$dest = buildNewDestUrlSource ( $_SERVER, "signin.php" );
	header ( "Location: " . $dest );
}
	
toHttps ();

if (isset ( $_COOKIE ["error"] )) {
	$error = $_COOKIE ["error"];
	setcookie ( "error", "", time () - 3600 );
}
?>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="it">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<title>Hall Reservation</title>
<script type="text/javascript" src="MyScripts.js"></script>
<link href="MyStyle.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="navbar">
		<ul class="navbar">
			<li><a id="navlink" href="index.php">Home</a></li>
			<li><a id="navlink" href="signup.php">Sign Up</a></li>
			<li><a id="navlink" href="signin.php">Sign In</a></li>
			<li><a id="navlink" href="logout.php">Log Out</a></li>
			<li><a id="navlink" href="personalreservation.php">Personal Page</a></li>
		</ul>
	</div>
	<div class="header">
		<div id="title">
			<h1>Hotel Conference Hall Booking Site</h1>
		</div>
		<div id="page">
			<h2>Personal Page</h2>
		</div>
	</div>
	<noscript id="alert">Sorry, your browser does not support or has disabled
		Javascript! Please consider changing browser or turning it back on.</noscript>
	<div class="content">
		<h3>Current bookings</h3>
		<table class="table">
			<tr id="header">
				<th>#</th>
				<th>Number Of Participants</th>
				<th>Start Time</th>
				<th>End Time</th>
				<th>Author</th>
			</tr>		
				<?php
				$query = "SELECT NOfParticipants, StartTime, EndTime, Booker FROM bookings ORDER BY NOfParticipants DESC";
				$res = getQuery ( $db, $query );
				
				$row = mysqli_fetch_array ( $res );
				$i = 0;
				while ( $row != NULL ) {
					?>    
	    <tr id="data">
				<td><?php
					echo ($i);
					$i ++;
					?></td>
				<td><?php
					echo ($row ["NOfParticipants"]);
					?></td>
				<td><?php
					$st = formatTimeHhMm ( $row ["StartTime"] );
					echo ($st);
					?></td>
				<td><?php
					$et = formatTimeHhMm ( $row ["EndTime"] );
					echo ($et);
					?></td>
				<td><?php
					echo ($row ["Booker"]);
					?></td>
			</tr>

    <?php
					$row = mysqli_fetch_array ( $res );
				}
				
				mysqli_free_result ( $res );
				?>
    </table>
		<br>
		<hr>
		<br>
		<h3>Bookings associated to user <?php echo($_SESSION["S220352user"]);?></h3>
		<form name="UserBooking" action="cancel.php" method="get">
			<table class="table">
				<tr id="header">
					<th>#</th>
					<th>Number Of Participants</th>
					<th>Start Time</th>
					<th>End Time</th>
					<th>Cancel</th>
				</tr>		
				<?php
				$query = "SELECT BId, NOfParticipants, StartTime, EndTime FROM bookings WHERE Booker='" . $_SESSION ["S220352user"] . "' ORDER BY NOfParticipants DESC";
				$res = getQuery ( $db, $query );
				
				$row = mysqli_fetch_array ( $res );
				$i = 0;
				while ( $row != NULL ) {
					?>    
	    <tr id="data">
					<td><?php
					echo ($i);
					$i ++;
					?></td>
					<td><?php
					echo ($row ["NOfParticipants"]);
					?></td>
					<td><?php
					$st = formatTimeHhMm ( $row ["StartTime"] );
					echo ($st);
					?></td>
					<td><?php
					$et = formatTimeHhMm ( $row ["EndTime"] );
					echo ($et);
					?></td>
				<?php
					echo ('<td><input type="checkbox" name="toCancel[]" value=' . $row ["BId"] . '></td>');
					?>
			</tr>

    <?php
					$row = mysqli_fetch_array ( $res );
				}
				
				mysqli_free_result ( $res );
				?>
    </table>
			<input type="submit" value="Cancel selected bookings"> 
			<script>
			document.write('<input type="button" onclick="javascript:deleteSelected(\'UserBooking\',false);" value="Delete selection"> ');
			document.write('<input type="button" onclick="javascript:deleteSelected(\'UserBooking\',true);"	value="Select all">');
			</script>
		</form>
		<br>
		<hr>
		<br>
    	<?php
					if (isset ( $error )) {
						if (! strcmp ( $error, "invalid" )) {
							echo ("
									<h4 id='alert'>Error! Impossible to issue such booking. Maximum capacity reached!<h4>
									");
						} else {
							echo ("
									<h4 id='alert'>Error! Incorrect input! Check and insert data again!</h4>
									");
						}
					}
					?>
		<form action="new_booking.php" method="get" class="login"
			name="new_booking" onsubmit="return checkNewBooking()">
			<h3>Issue a new booking</h3>
			<label> <span>Participants: </span> <input type="text"
				id="#OfParticipants" name="#OfParticipants"
				placeholder="Insert # of participants"
				title="Insert # of participants">
			</label> <label> <span>Start Time: </span> <select name="startHour"
				title="Insert Start Hour">
					<option value="00">00</option>
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
			</select> <select name="startMinutes" title="Insert Start Minutes">
					<option value="00">00</option>
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
					<option value="24">24</option>
					<option value="25">25</option>
					<option value="26">26</option>
					<option value="27">27</option>
					<option value="28">28</option>
					<option value="29">29</option>
					<option value="30">30</option>
					<option value="31">31</option>
					<option value="32">32</option>
					<option value="33">33</option>
					<option value="34">34</option>
					<option value="35">35</option>
					<option value="36">36</option>
					<option value="37">37</option>
					<option value="38">38</option>
					<option value="39">39</option>
					<option value="40">40</option>
					<option value="41">41</option>
					<option value="42">42</option>
					<option value="43">43</option>
					<option value="44">44</option>
					<option value="45">45</option>
					<option value="46">46</option>
					<option value="47">47</option>
					<option value="48">48</option>
					<option value="49">49</option>
					<option value="50">50</option>
					<option value="51">51</option>
					<option value="52">52</option>
					<option value="53">53</option>
					<option value="54">54</option>
					<option value="55">55</option>
					<option value="56">56</option>
					<option value="57">57</option>
					<option value="58">58</option>
					<option value="59">59</option>
			</select>
			</label> <label> <span>End Time: </span> <select name="endHour"
				title="Insert End Hour">
					<option value="00">00</option>
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
			</select> <select name="endMinutes" title="Insert End Minutes">
					<option value="00">00</option>
					<option value="01">01</option>
					<option value="02">02</option>
					<option value="03">03</option>
					<option value="04">04</option>
					<option value="05">05</option>
					<option value="06">06</option>
					<option value="07">07</option>
					<option value="08">08</option>
					<option value="09">09</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="21">21</option>
					<option value="22">22</option>
					<option value="23">23</option>
					<option value="24">24</option>
					<option value="25">25</option>
					<option value="26">26</option>
					<option value="27">27</option>
					<option value="28">28</option>
					<option value="29">29</option>
					<option value="30">30</option>
					<option value="31">31</option>
					<option value="32">32</option>
					<option value="33">33</option>
					<option value="34">34</option>
					<option value="35">35</option>
					<option value="36">36</option>
					<option value="37">37</option>
					<option value="38">38</option>
					<option value="39">39</option>
					<option value="40">40</option>
					<option value="41">41</option>
					<option value="42">42</option>
					<option value="43">43</option>
					<option value="44">44</option>
					<option value="45">45</option>
					<option value="46">46</option>
					<option value="47">47</option>
					<option value="48">48</option>
					<option value="49">49</option>
					<option value="50">50</option>
					<option value="51">51</option>
					<option value="52">52</option>
					<option value="53">53</option>
					<option value="54">54</option>
					<option value="55">55</option>
					<option value="56">56</option>
					<option value="57">57</option>
					<option value="58">58</option>
					<option value="59">59</option>
			</select>
			</label> <input type="submit" id="confirm" value="Confirm">
		</form>
	</div>
</body>
</html>