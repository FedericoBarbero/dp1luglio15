<?php
include 'functions.php';
if(!checkSession())
{
	$https= false;
	$dest = buildNewDestUrlSource ( $_SERVER, "signin.php" );
	header ( "Location: " . $dest );
}
$user = "";
if(!isset($_SESSION["S220352user"]))
{
	session_unset();
	session_destroy();
	$https=false;
	$dest = getHomeUrl($_SERVER);
	header("Location: " . $dest);
	exit();
}
$part = $_GET["#OfParticipants"];
$start = $_GET["startHour"].":".$_GET["startMinutes"].":00";
$end = $_GET["endHour"].":".$_GET["endMinutes"].":00";
$u = $_SESSION["S220352user"];

if($part=="" || !ctype_digit($part) || $part > 100 || $part < 0 || toMinutes($start)>=toMinutes($end))
{
	setcookie("error","incorrect");
	$dest = "personalreservation.php";
	//$dest = "personalreservation.php?error='incorrect'";
	$dest=buildNewDestUrl($_SERVER, $dest);
	header("Location: ".$dest);
	exit();
}

if(isBFeasible($start, $end, $part))
{
	$query = "INSERT INTO bookings(NofParticipants, StartTime,EndTime, Booker) VALUES('".$part."','".$start."','".$end."','".$u."')";
	getQuery($db, $query);
	$dest = "personalreservation.php";
}
else {
	setcookie("error","invalid");
	$dest = "personalreservation.php";
}
$dest=buildNewDestUrl($_SERVER, $dest);
header("Location: ".$dest);
exit();
?>
