<?php
include 'functions.php';
if (! isset ( $_COOKIE ['testCookie'] )) {
	setcookie ( 'testCookie', 'enabled' );
	$dest = buildNewDestUrlSource ( $_SERVER, "check.php" );
	header ( "Location: " . $dest );
}

session_start ();

if (! isset ( $_SESSION ["S220352user"] )) {
	session_unset ();
	session_destroy ();
	if(!isset($_GET["justLogged"])){
		$dest = getHomeUrl ( $_SERVER );
		header ( "Location: " . $dest );
	}
} else {
	
	session_unset ();
	session_destroy ();
	toHttp();
}
?>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="it">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<title>Hall Reservation</title>
<script type="text/javascript" src="MyScripts.js"></script>
<link href="MyStyle.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
			function backBtOnClick() {
				var home = document.getElementById("url").value;
				window.location.assign(home);
			}
		</script>
</head>
<body>
	<div class="navbar">
		<ul class="navbar">
			<li><a id="navlink" href="index.php">Home</a></li>
			<li><a id="navlink" href="signup.php">Sign Up</a></li>
			<li><a id="navlink" href="signin.php">Sign In</a></li>
			<li><a id="navlink" href="logout.php">Log Out</a></li>
			<li><a id="navlink" href="personalreservation.php">Personal Page</a></li>
		</ul>
	</div>
	<div class="header">
		<div id="title">
			<h1>Hotel Conference Hall Booking Site</h1>
		</div>
		<div id="page">
			<h2>Logout</h2>
		</div>
	</div>
	<div class="content">
		<h3>You are now logged out!</h3>
		<script>
		 document.write('<button id="back" name="back" onclick="backBtOnClick();">Go to the	Home Page</button>');
		</script>
		<noscript id="alert">Sorry, your browser does not support or has
			disabled Javascript! Please consider changing browser or turning it
			back on.</noscript>
						<?php
						$url = getHomeUrl ( $_SERVER );
						echo ('<input type="hidden" name="url" id="url" value="' . $url . '">');
						
						?>
				
	</div>
</body>
</html>