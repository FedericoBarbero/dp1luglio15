<?php
include 'functions.php';
if (! checkSession ()) {
	$https = false;
	$dest = buildNewDestUrlSource ( $_SERVER, "signin.php" );
	header ( "Location: " . $dest );
}
$user = "";
if (! isset ( $_SESSION ["S220352user"] )) {
	session_unset ();
	session_destroy ();
	$dest = getHomeUrl ( $_SERVER );
	header ( "Location: " . $dest );
	exit ();
}

$user = $_SESSION ["S220352user"];

$tc = $_GET ['toCancel'];
if (! empty ( $tc )) {
	$N = count ( $tc );
	for($i = 0; $i < $N; $i ++) {
		$query = "DELETE FROM bookings WHERE BId = '" . $tc [$i] . "' AND Booker='" . $user . "'";
		$res = getQuery ( $db, $query );
	}
	
	$dest = buildNewDestUrl ( $_SERVER, "personalreservation.php" );
	header ( "Location: " . $dest );
	exit ();
}
?>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="it">
<head>
<meta content="text/html; charset=utf-8" http-equiv="content-type">
<title>Hall Reservation</title>
<script type="text/javascript" src="MyScripts.js"></script>
<link href="MyStyle.css" rel="stylesheet" type="text/css">
<style>
h3 {
	color: red;
	font-family: Verdana, Arial, sans-serif;
}
</style>
<script type="text/javascript">
			function backBtOnClick() {
				var home = document.getElementById("url").value;
				window.location.assign(home);
			}
		</script>
</head>
<body>
	<div class="header">
		<div id="title">
			<h1>Hotel Conference Hall Booking Site</h1>
		</div>
		<div id="page">
			<h2>Error!</h2>
		</div>
	</div>
	<div id="content">
		<noscript id="alert">Sorry, your browser does not support or has
			disabled Javascript! Please consider changing browser or turning it
			back on.</noscript>

		<h3>Error in cancelling your bookings!</h3>
		<script>
		 document.write('<button id="back" name="back" onclick="backBtOnClick();">Go to the	Home Page</button>');
		</script>
						<?php
						$url = getHomeUrl ( $_SERVER );
						echo ('<input type="hidden" name="url" id="url" value="' . $url . '">');
						
						?>
				
			</div>
</body>
</html>